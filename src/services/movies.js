import request from '../utils/request';

export function fetchMovies() {
    return request("https://api.themoviedb.org/3/movie/now_playing?api_key=")
}