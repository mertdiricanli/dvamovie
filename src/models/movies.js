import { fetchMovies } from '../services/movies';
export default {
    namespace: 'movies',
    state: {
        items: [],
        loading: false,
        error: false
    },
    effects: {
        * fetchMovies({ payload }, { put, call }) {
            yield put({
                type: "fetchMoviesStarted"
            });
            try {
                const movies = yield call(fetchMovies);
                yield put({
                    type: "fetchMoviesHandler",
                    payload: movies.data.results // or movies.data etc.
                });
                yield put({
                    type: "fetchMoviesSuccess"
                });
            } catch (error) {
                yield put({
                    type: "fetchMoviesError",
                    payload: error
                });
            }
        }
    },
    reducers: {
        'fetchMoviesStarted'(state) {
            return {
                ...state,
                loading: true
            }
        },
        'fetchMoviesHandler'(state, action) {
            return {
                ...state,
                items: action.payload
            }
        },
        'fetchMoviesSuccess'(state) {
            return {
                ...state,
                loading: false
            }
        },
        'fetchMoviesError'(state) {
            return {
                ...state,
                loading: false,
                error: true
            }
        }
    }

}