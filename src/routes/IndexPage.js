import React, { Component } from 'react';
import { connect } from 'dva';
import styles from './IndexPage.css';

class IndexPage extends Component {
  componentDidMount() {
    const { dispatch } = this.props;

    dispatch({ type: 'movies/fetchMovies' })
  }

  render() {
    let error = null;
    if (this.props.movies.error) {
      error = <p>An error occured!</p>;
    }

    return (
      <div className={styles.container}>
        {error}
        {this.props.movies.loading ? "Loading" : <ul>{this.props.movies.items.map(movie => <li key={movie.id}>{movie.title}</li>)}</ul>}
      </div>
    );
  }
};

export default connect(({ movies }) => ({ movies }))(IndexPage);